import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraphQLModule } from '@nestjs/graphql';
import { PeopleEntity } from './people/people.entity';
import { PeopleModule } from './people/people.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb+srv://LouRectoret:Sibovv12@cluster-dev-mxv.lpwsv.mongodb.net/mxv?retryWrites=true&w=majority',
      synchronize: true,
      useUnifiedTopology: true,
      entities: [
        PeopleEntity
      ]
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: true
    }),
    PeopleModule
  ],
})
export class AppModule {}
