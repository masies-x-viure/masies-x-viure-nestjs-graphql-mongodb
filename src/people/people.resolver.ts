import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PeopleType } from './people.type';
import { PeopleService } from './people.service';
import { CreatePeopleInput } from './people.input';

@Resolver(of => PeopleType)
export class PeopleResolver {
  constructor(
    private peopleService: PeopleService
  ) {}

  @Mutation(returns => PeopleType)
  createPeople(
    @Args('createPeopleInput') createPeopleInput: CreatePeopleInput
  ) {
    return this.peopleService.createPeople(createPeopleInput)
  }

  @Query(returns => [PeopleType])
  getAllPeople() {
    return this.peopleService.getAllPeople()
  }
}
