import { Field, InputType } from '@nestjs/graphql';
import { IsBoolean, IsEmail, IsNumber, IsString, MinLength } from 'class-validator';

@InputType()
export class CreatePeopleInput {
  @MinLength(3)
  @Field()
  name: string;

  @IsEmail()
  @Field()
  email: string;

  @IsString({ each: true })
  @Field(type => [String])
  locations: string[];

  @IsString({ each: true })
  @Field(type => [String])
  sharedAreas: string[];

  @IsString({ each: true })
  @Field(type =>[String])
  skills: string[];

  @IsNumber()
  @Field()
  savings: number;

  @IsBoolean()
  @Field()
  prevExperience: boolean;

  @IsBoolean()
  @Field()
  housingCoop: boolean;

  @IsBoolean()
  @Field()
  workCoop: boolean;
}
