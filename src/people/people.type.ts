import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class PeopleType {
  @Field(type => ID)
  id: string;

  @Field()
  name: string;


  @Field()
  email: string;

  @Field(type => [String])
  locations: string[];

  @Field(type => [String])
  sharedAreas: string[];

  @Field(type =>[String])
  skills: string[];

  @Field()
  savings: number;

  @Field()
  prevExperience: boolean;

  @Field()
  housingCoop: boolean;

  @Field()
  workCoop: boolean;
  //
  // @Field()
  // createdAt: Date;
}
