import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PeopleService } from './people.service';
import { PeopleResolver } from './people.resolver';
import { PeopleEntity } from './people.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([PeopleEntity]),
  ],
  providers: [
    PeopleService,
    PeopleResolver
  ],
})
export class PeopleModule {}
