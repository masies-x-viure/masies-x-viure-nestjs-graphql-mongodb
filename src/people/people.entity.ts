import { Column, Entity, ObjectIdColumn, PrimaryColumn } from 'typeorm';

@Entity()
export class PeopleEntity {
  @ObjectIdColumn()
  _id: string

  @PrimaryColumn()
  id: string

  @Column()
  name: string

  @Column()
  email: string

  @Column()
  locations: string[]

  @Column()
  sharedAreas: string[]

  @Column()
  skills: string[]

  @Column()
  savings: number

  @Column()
  prevExperience: boolean

  @Column()
  housingCoop: boolean

  @Column()
  workCoop: boolean

  // @Column()
  // createdAt: Date
}
