import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { v4 as uuid } from 'uuid';
import { Repository } from 'typeorm';
import { PeopleEntity } from './people.entity';
import { CreatePeopleInput } from './people.input';

@Injectable()
export class PeopleService {
  constructor(
    @InjectRepository(PeopleEntity) private peopleRepository: Repository<PeopleEntity>
  ) {}

  async createPeople(createPeopleInput: CreatePeopleInput): Promise<PeopleEntity> {
    const people = await this.peopleRepository.create({
      id: uuid(),
      ...createPeopleInput,
    })

    return await this.peopleRepository.save(people)
  }

  async getAllPeople(): Promise<PeopleEntity[]> {
    return await this.peopleRepository.find()
  }
}
