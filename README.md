<p align="center">
  <a href="http://masiesxviure.cat/" target="blank"><img src="https://res.cloudinary.com/despertaweb/image/upload/v1603889001/Masies%20x%20Viure/Logo/MxV_LOGO_M_cfcbjg.png" width="320" alt="Masies x Viure Logo" /></a>
</p>



# Masies x Viure 

És l’app per a persones i grups que volen crear una cooperativa d’habitatge al món rural fent possible una re-ruralització conscient i respectuosa amb l'entorn i els seus habitants(plantes, animals i homosàpiens).

També va dirigida a cooperatives locals que puguin oferir algun servei a les cooperatives:
- Assessorament: Legal, gestió de grups i gestió de conflictes, definició d'estatuts...
- Habitatge: Propietàris que vulguis facilitar l'ús d'habitatges i terres en modalitats de lloguer, masoveria, venda...
- Bioconstrucció i sostenibilitat: Tècnics en bioconstrucció, arquitèctes, proveïdors de materials sostenibles, instal·ladors d'energies verdes...
- Finançament: Banca ètica, grups locals de crèdit...
 
## Descripció
Aquest projecte està fet amb [Nest](https://github.com/nestjs/nest) & TypeScript & GraphQL & MongoDB

## Instal·lació

```bash
$ yarn
```

## Alçar l'App

```bash
# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Suport i Contacte 
**Podeu contactar-me aquí:** 

- masiesxviure@protonmail.com

- [Lou Rectoret](https://twitter.com/LouRectoret)
## License

  Aquest codi és per a tothom, sempre i quan se'n faci un ús pel bé comú i per a combatre desigualtats al món. En qualsevol cas, si dubteu, escriviu a:
- masiesxviure@protonmail.com
